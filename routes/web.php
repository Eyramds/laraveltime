<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/seances', 'SeancesController@seances')->name('seances');
Route::post('/add_seance', 'SeancesController@add_seance')->name('add_seance');

Route::get('/wall', 'WallController@index')->name('wall');
Route::post('/write', 'WallController@write')->name('write');

Route::get('/movies', 'MoviesController@index')->name('movies');
Route::get('/series', 'SeriesController@index')->name('series');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/show', 'HomeController@movies')->name('movies');